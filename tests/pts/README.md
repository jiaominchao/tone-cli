# pts
## Description
The **Phoronix Test Suite** is the most comprehensive testing and benchmarking
platform available for Linux, Solaris, macOS, Windows, and BSD operating
systems. The Phoronix Test Suite allows for carrying out tests in a fully
automated manner from test installation to execution and reporting. All tests
are meant to be easily reproducible, easy-to-use, and support fully automated
execution. The Phoronix Test Suite is open-source under the GNU GPLv3 license
and is developed by Phoronix Media in cooperation with partners.

## Homepage
https://openbenchmarking.org

## Version
10.8.4

## Category
performance

## Parameters
- bench: 
    * mafft
    * c-ray
    * cachebench
    * stream
    * ramspeed
    * fio
    * fsmark
    * blogbench
    * tiobench
    * dbench
    * iozone
    * postmark
    * network-loopback
    * sysbench
    * apache
    * nginx
    * redis
    * sqlite
    * pgbench
    * phpbench
    * compilebench
    * compress-7zip
    * compress-gzip
    * openssl
    * john-the-ripper

## Results
Stream-2013-01-17-Type-Copy: 45350.1 MB/s
Stream-2013-01-17-Type-Scale: 31983.6 MB/s
CacheBench-Test-Read: 2950.88451 MB/s
CacheBench-Test-Write: 24105.337907 MB/s
IOzone-3.465-Record-Size-1MB---File-Size-4GB---Disk-Test-Read-Performance: 7307.19 MB/s
PostgreSQL-15-Scaling-Factor-1---Clients-1---Mode-Read-Only-TPS: 31324.5
OpenSSL-3.0-Algorithm-SHA256: 1734025890.0 byte/s
7-Zip-Compression-22.01-Test-Compression-Rating: 19150.5 MIPS

## Manual Run
./phoronix-test-suite install sqlite
./phoronix-test-suite run sqlite
