#!/usr/bin/env python
import sys
import re

reg = re.compile(r'^#\s+(Avg\s+Latencies|Max\s+Latencies):\s+(\w.*)')
with sys.stdin as stdin:
    for line in stdin.readlines():
        result = reg.search(line)
        if result:
            (key, value) = result.groups()
            print("{}: {}".format(key, value))
