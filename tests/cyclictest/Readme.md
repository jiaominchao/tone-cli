# cyclictest
## Description
Cyclictest accurately and repeatedly measures the difference between a thread's intended wake-up time and the time at which it actually wakes up in order to provide statistics about the system's latencies. It can measure latencies in real-time systems caused by the hardware, the firmware, and the operating system.

The original test was written by Thomas Gleixner (tglx), but several people have subsequently contributed modifications. Cyclictest is currently maintained by Clark Williams and John Kacur and is part of the test suite rt-tests.

## Homepage
https://wiki.linuxfoundation.org/realtime/documentation/howto/tools/cyclictest/start

## Version

## Category
performance

## Parameters
- l: number of loops, default=0(endless)
- p: priority of highest prio thread
- t: one thread per available processor
- h: dump a latency histogram to stdout after the run US is the max latency time to be tracked in microseconds.
     This option runs all threads at the same priority.
- m: lock current and future memory allocations
- s: use sys_nanosleep and sys_setitimer
- a: Run thread #N on processor #N, if possible, or if CPUSET given, pin threads to that set of processors in    
     round-robin order.  E.g. -a 2 pins all threads to CPU 2, but -a 3-5,0 -t 5 will run the first and fifththreads on CPU (0),thread #2 on CPU 3, thread #3 on CPU 4, and thread #5 on CPU 5.


## Results
Avg Latencies: 00002 00002 00002 00002 00002 00002
Max Latencies: 00010 00009 00009 00009 00009 00008

## Manual Run

