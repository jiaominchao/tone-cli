# keentune
## Description
Keentune is a full stack intelligent optimization product of operating system driven by two wheels of AI algorithm and expert knowledge base. It provides lightweight and cross platform one click performance optimization for mainstream operating systems, optimizes best performance of applications in an intelligent customized operating environment.

## Version
NA

## Category
functional

## Parameters
- test_branch:master, git branch of keentuned code(acops-new)

## Results
testcase: Pass
testcase: Fail

## Manual Run
keentuned/test/KeenTune_UI