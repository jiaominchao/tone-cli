#!/usr/bin/env python
import sys
import re
import json

# Handle the input pipe
with sys.stdin as stdin:
    pass

fio_result_file = "fio-ceprei.output"
with open(fio_result_file) as fh:
    data = json.load(fh)

job_info = data['jobs'][0]

for unit in ('us', 'ms'):
    key = "latency_{}".format(unit)
    for k, v in job_info[key].items():
        if v != 0:
            print("latency_{}{}: {} %".format(k, unit, v))

allios = 0
for op in ('read', 'write'):
    ops_info = job_info[op]
    if ops_info['bw'] == 0:
        continue
    print("{}_bw: {} MBps".format(op, ops_info['bw'] / 1024.0))
    print("{}_iops: {}".format(op, ops_info['iops']))
    if 'clat' in ops_info:
        clat = ops_info['clat']
        unit = 'us'
    else:
        clat = ops_info['clat_ns']
        unit = 'ns'
    # TODO: the unit for mean not us for clat_ns exists
    print("{}_clat_mean: {} {}".format(op, clat['mean'], unit))
    print("{}_clat_stddev: {}".format(op, clat['stddev']))
    percentile = clat['percentile']
    for percent in ('90.000000', '95.000000', '99.000000'):
        if percent in percentile:
            if percentile[percent] != 0:
                print("{}_clat_{}%: {} {}".format(
                    op,
                    int(float(percent)),
                    percentile[percent],
                    unit
                ))
    if 'slat' in ops_info:
        slat = ops_info['slat']
        unit = 'us'
    else:
        slat = ops_info['slat_ns']
        unit = 'ns'
    if slat['mean'] != 0:
        print("{}_slat_mean: {} {}".format(op, slat['mean'], unit))
    if slat['stddev'] != 0:
        print("{}_slat_stddev: {}".format(op, slat['stddev']))

    allios += ops_info['total_ios']
print("workload: {}".format(allios))
