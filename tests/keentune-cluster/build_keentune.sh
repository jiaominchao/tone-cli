#!/bin/bash
keentune_code_dir=/tmp/keentune-cluster-tmp/
code_source=$1
test_branch=$2
role=$3

keentune_conf=$keentune_code_dir/acops-new/keentuned/keentuned.conf
server_conf=$keentune_code_dir/acops-new/test/common.py

log_suffix="$code_source-$test_branch-$(date +"%Y-%m-%d-%H-%M-%S")"


clear_keentune_env()
{
    rm -rf /usr/local/lib/python3.6/site-packages/keentune*
    rm -rf /usr/local/lib/python3.6/site-packages/brain
    rm -rf /usr/local/lib/python3.6/site-packages/target
    rm -rf /usr/local/lib/python3.6/site-packages/bench
    rm -f /usr/local/bin/keentune*
    ps -ef|grep -E 'keentuned|keentune-brain|keentune-target|keentune-bench'|grep -v grep|awk '{print $2}'| xargs -I {} kill -9 {}
}

sed_ip()
{
    sed -i "s/BRAIN_IP.*=.*/BRAIN_IP = ${CLIENT_3}/g" $keentune_conf
	sed -i "s/TARGET_IP.*=.*/TARGET_IP = ${CLIENT_1}/g" $keentune_conf
	sed -i "s/BENCH_SRC_IP.*=.*/BENCH_SRC_IP = ${CLIENT_2}/g" $keentune_conf
	sed -i "s/BENCH_DEST_IP.*=.*/BENCH_DEST_IP = ${CLIENT_1}/g" $keentune_conf
	
	sed -i "s/keentuned_ip=.*/keentuned_ip=\"${SERVER}\"/g" $server_conf
	sed -i "s/brain_ip=.*/brain_ip=\"${CLIENT_3}\"/g" $server_conf
	sed -i "s/target_ip=.*/target_ip=\"${CLIENT_1}\"/g" $server_conf
	sed -i "s/bench_ip=.*/bench_ip=\"${CLIENT_2}\"/g" $server_conf
}

get_keentune_code()
{
	cd $keentune_code_dir
	[ -d $keentune_code_dir/acops-new_$code_source ] || return 1
	
	local ret=0
    rm -rf acops-new
	cp acops-new_${code_source} acops-new -r
	cd acops-new
	
    if [ "$code_source" == "gitee" ];then
        cd keentuned && git checkout $test_branch -f || ret=1
        cp test ../ -r
        cd ..

        mv keentune_brain keentune-brain
        cd keentune-brain && git checkout $test_branch -f || ret=1
        cd ..

        mv keentune_target keentune-target
        cd keentune-target && git checkout $test_branch -f || ret=1
        cd ..

        mv keentune_bench keentune-bench
        cd keentune-bench && git checkout $test_branch -f || ret=1
        cd ..
	else
		return 1
    fi
	
	cd $keentune_code_dir/acops-new/test
	if [ "$role" == "SERVER" ];then
		sed_ip
	fi
	
	return $ret
}

build_keentune()
{
	get_keentune_code || return 1
	
	local ret=0
    cd $keentune_code_dir/acops-new

	if [ "$role" == "CLIENT1" ];then
		cd keentune-target
		python3 setup.py install || ret=1
		keentune-target > keentune-target-$log_suffix 2>&1 &
		cd ../keentune-bench
		python3 setup.py install || ret=1
		keentune-bench > keentune-bench-$log_suffix 2>&1 &
	elif [ "$role" == "CLIENT2" ];then
		cd keentune-bench
		python3 setup.py install || ret=1
		keentune-bench > keentune-bench-$log_suffix 2>&1 &
		cd ../keentune-target
		python3 setup.py install || ret=1
		keentune-target > keentune-target-$log_suffix 2>&1 &
	elif [ "$role" == "CLIENT3" ];then
		cd keentune-brain
		python3 setup.py install || ret=1
		keentune-brain > keentune-brain-$log_suffix 2>&1 &
		cd ../keentune-target
		python3 setup.py install || ret=1
		keentune-target > keentune-target-$log_suffix 2>&1 &
		cd ../keentune-bench
		python3 setup.py install || ret=1
		keentune-bench > keentune-bench-$log_suffix 2>&1 &
	else
		cd keentuned
		go mod vendor
		./keentuned_install.sh || ret=1
		keentuned > keentuned-$log_suffix 2>&1 &
		
		cd ../keentune-brain
		python3 setup.py install || ret=1
		keentune-brain > keentune-brain-$log_suffix 2>&1 &

		cd ../keentune-target
		python3 setup.py install || ret=1
		keentune-target > keentune-target-$log_suffix 2>&1 &

		cd ../keentune-bench
		python3 setup.py install || ret=1
		keentune-bench > keentune-bench-$log_suffix 2>&1 &
	fi
	
	return $ret
}

clear_keentune_env
build_keentune
exit $?
