# anolis-ci-test
## Description
Anolis CI test suite containes seral test cases for checking
code changes in pull request, including licence check, code
check, local build, make check, install test, etc. 

Following env variables are required:
export GIT_REPO=<git repo url>
export GIT_BRANCH=<git branch>
export GIT_PRID=<pull request id>

## Homepage
N/A

## Version
0.1

## Category
Functional

## Parameters

- testcase : separate test case for ci check

## Results

```
check_license: pass
check_specfile: warning
check_codestyle: warning
pkg_smoke_test: pass
check_abi_diff: fail
...
```
