# Avaliable environment:
#
# Download variable:
# WEB_URL=
# GIT_URL=
TESTCASE_NAME_LIST=${TESTCASE_NAME_LIST:-""}

setup ()
{
    :
}

run ()
{
    cd $TONE_BM_RUN_DIR/ancert
    echo "Current working dir is `pwd`"

    echo "selected categroy is $category"
    option="--report --tone"
    if [ -n "$TESTCASE_NAME_LIST" ]; then
        echo "run tests case: $TESTCASE_NAME_LIST"
        option="--cases $TESTCASE_NAME_LIST $option"
    fi
    python3 ancert --category $category $option
}

teardown ()
{
    :
}

parse ()
{
    $TONE_BM_SUITE_DIR/parse.awk
}
