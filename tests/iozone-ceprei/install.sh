WEB_URL="http://www.iozone.org/src/current/iozone3_430.tar"
# BRANCH="20210927"


if echo "ubuntu debian uos kylin" | grep $TONE_OS_DISTRO; then  
         DEP_PKG_LIST=""

else
         DEP_PKG_LIST=""

fi


build()
{
    cp -r  $TONE_BM_CACHE_DIR/iozone3_430.tar  $TONE_BM_RUN_DIR/
    cd $TONE_BM_RUN_DIR/
    tar -xvf iozone3_430.tar
    cd ./iozone3_430/src/current
    pwd
    if [[ $(/bin/arch) == 'x86_64' ]];then
        make linux-AMD64
    else
        make linux
    fi
}

install()
{
    echo "installed successful"
}
