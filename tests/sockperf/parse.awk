#!/usr/bin/awk -f

/^sockperf: .*avg-latency=/ {
    split($0,results,"[ |)|=|>|(]*")
    printf("avg-lat: %s\n",results[4])
    printf("std-dev: %s\n",results[6])
    next
}

/^sockperf: Summary: Message Rate is [0-9]/ {
    printf("msg_per_sec: %s\n",$6)
    next
}

/^sockperf: Summary: BandWidth is [0-9]/ {
    printf("BandWidth_MBps: %s\n",$5)
    next
}
