#!/bin/sh

perf_blacklist=$TONE_BM_SUITE_DIR/perf.blacklist

get_kernel_info()
{
    kernel_ver=$(uname -r | awk -F '.' '{print$1"."$2}')
    os_ver=$(uname -r | awk -F '.' '{print$(NF-1)}')
    arch=$(uname -m)
}

get_kernel_info

prepare_for_blacklist()
{
    if [ x"$os_ver" == x"an8" -a x"$kernel_ver" == x"4.19" ]; then
        # https://bugzilla.openanolis.cn/show_bug.cgi?id=1107
        grep -q "^Setup struct perf_event_attr$" $perf_blacklist || echo "Setup struct perf_event_attr" >>$perf_blacklist
    fi

    if [ x"$os_ver" == x"an8" -a x"$kernel_ver" == x"5.10" -a x"$arch" == x"aarch64" ]; then
        # https://bugzilla.openanolis.cn/show_bug.cgi?id=2466
        grep -q "vmlinux symtab matches kallsyms$" $perf_blacklist || echo "vmlinux symtab matches kallsyms" >>$perf_blacklist
    fi

    if [ x"$os_ver" == x"an7" -a x"$kernel_ver" == x"4.19" ]; then
        # https://bugzilla.openanolis.cn/show_bug.cgi?id=2595
        grep -q "^LLVM search and compile$" $perf_blacklist || echo "LLVM search and compile" >>$perf_blacklist
    fi

    if [ -f $perf_blacklist ];then
        while read param
        do
            if [ x"$param" != x ];then
                skip_case=$(perf test list "$param" 2>&1 | awk -F ':' '{print $1}' | head -n 1)
                skip_case_list=$skip_case_list','$skip_case
            fi
        done <$perf_blacklist
    fi

}

prepare_hw_skip_list()
{
    cat > hardware.skiplist << EOF
Object code reading
x86 rdpmc
Convert perf time to TSC
EOF

    while read arg
    do
        skip_test=$(perf test list "$arg" 2>&1 | awk -F ':' '{print $1}')
        skip_test_list=$skip_test_list','$skip_test
    done <hardware.skiplist
}

run()
{
    perf list | grep -q 'Hardware event' || prepare_hw_skip_list
    prepare_for_blacklist
    perf test -s "$skip_test_list""$skip_case_list" 2>&1
}

parse()
{
    "$TONE_BM_SUITE_DIR"/parse.py
}
