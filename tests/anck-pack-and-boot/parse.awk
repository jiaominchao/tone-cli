#!/usr/bin/awk -f

/^====PASS/ {
        printf("%s: Pass\n",$2)
}

/^====FAIL:/ {
        printf("%s: Fail\n",$2)
}

/^====SKIP:/ {
        printf("%s: Skip\n",$2)
}
