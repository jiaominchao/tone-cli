#!/bin/bash

pass()
{
	echo "====PASS: $*"
}

fail()
{
	echo "====FAIL: $*"
}

skip()
{
	echo "====SKIP: $*"
}

anck_rpm_build()
{
	[ -n "$KERNEL_CI_REPO_URL" ] || {
		echo "Error: KERNEL_CI_REPO_URL is not set"
		return 1
	}
	[ -n "$KERNEL_CI_REPO_BRANCH" ] || {
		echo "Error: KERNEL_CI_REPO_BRANCH is not set"
		return 1
	}
	[ -n "$KERNEL_CI_PR_ID" ] || {
		echo "Error: KERNEL_CI_PR_ID is not set"
		return 1
	}
	[ -n "$CK_BUILDER_REPO" ] || {
		CK_BUILDER_REPO="https://gitee.com/src-anolis-sig/ck-build.git"
	}
	[ -n "$CK_BUILDER_BRANCH" ] || {
		if echo "$KERNEL_CI_REPO_BRANCH" | grep -q "4.19"; then
			CK_BUILDER_BRANCH="an8-4.19"
		elif echo "$KERNEL_CI_REPO_BRANCH" | grep -q "5.10"; then
			CK_BUILDER_BRANCH="an8-5.10"
		elif echo "$KERNEL_CI_REPO_BRANCH" | grep -q "5.19"; then
			CK_BUILDER_BRANCH="an23-5.19"
			cat > /etc/yum.repos.d/5.19-koji.repo<<EOF
[koji]
name=koji
baseurl=http://8.131.87.1/kojifiles/repos/dist-an23-plus-build/latest/\$basearch/
enabled=1
gpgcheck=0
EOF
		else
			echo "Unsupported Kernel version branch: $KERNEL_CI_REPO_BRANCH"
			CK_BUILDER_BRANCH="an8-4.19"
		fi
	}

	cd $anck_build_dir || return 1
	anck_repo=$(basename $KERNEL_CI_REPO_URL .git)
	[ -d "$anck_repo" ] && rm -rf $anck_repo
	logger "git clone --depth 1 -b $KERNEL_CI_REPO_BRANCH $KERNEL_CI_REPO_URL"
	patch_url="${KERNEL_CI_REPO_URL/\.git/}/pulls/${KERNEL_CI_PR_ID}.patch"
	logger "wget $patch_url"
	logger "cat $(basename $patch_url)"
	cd $anck_repo
	git config user.email "test@test.com"
	git config user.name "test"
	logger "git am ../$(basename $patch_url)" || return 1

	cd ..
	ck_builder=$(basename $CK_BUILDER_REPO .git)
	[ -d "$ck_builder" ] && rm -rf $ck_builder
	logger "git clone -b $CK_BUILDER_BRANCH $CK_BUILDER_REPO" || return 1
	[ _"$ck_builder" != "_ck-build" ] && ln -s $ck_builder ck-build
	cd $ck_builder
	# for an8-4.19
	if grep ^srcdir build.sh | grep -q ck.git; then
		ln -sf ../${anck_repo} ck.git
	fi
	# for an8-5.10
	if grep ^srcdir build.sh | grep -q cloud-kernel; then
		ln -sf ../${anck_repo} cloud-kernel
	fi
	[ -d "outputs" ] && rm -rf outputs
	logger "yum-builddep -y kernel.spec"

	[ -n "$BUILD_EXTRA" ] || export BUILD_EXTRA="base"
	logger "sh build.sh" || return 1
	upload_archives $(find outputs -name *.rpm)
	return 0
}

anck_boot_test()
{
	[ -n "$REMOTE_HOST" ] || {
		echo "Error: REMOTE_HOST is not set"
		return 1
	}

	anck_rpms_dir="$anck_build_dir/ck-build/outputs/0"
	kver_new=$(find $anck_rpms_dir -name kernel-headers*.rpm | \
		head -n 1 | xargs \
		rpm -qp --queryformat="%{VERSION}-%{RELEASE}.%{ARCH}\n")

	remote_check || return 1
	remote_cmd "rm -rf /anck_rpms"
	logger "scp -r $anck_rpms_dir root@$REMOTE_HOST:/anck_rpms"
	remote_cmd "rpm -Uvh --force /anck_rpms/*.rpm"
	remote_cmd "reboot"
	sleep 30

	[ -n "$REBOOT_TIMEOUT" ] || REBOOT_TIMEOUT=600
	wait_time=30
	ret=1
	while [ "$wait_time" -lt "$REBOOT_TIMEOUT" ]
	do
		remote_check | grep -q $kver_new && ret=0 && break
		echo "Still waiting boot..."
		wait_time=$((wait_time + 30))
		sleep 30
	done
	remote_check
	if [ "$ret" -ne 0 ]; then
		echo "Error: failed to boot $kver_new in ${REBOOT_TIMEOUT}s"
	else
		echo "Succeed to boot new kernel!"
	fi
	return $ret
}

remote_check()
{
	logger "ping -c 1 $REMOTE_HOST" || return 1
	logger "timeout 5 ssh root@$REMOTE_HOST uname -r" || return 1
	return 0
}

remote_cmd()
{
	logger "ssh root@$REMOTE_HOST $*"
}

run()
{
	anck_build_dir="/anck_build"
	[ -d "$anck_build_dir" ] || mkdir -p $anck_build_dir

	if anck_rpm_build; then
		pass "anck_rpm_build"
		if anck_boot_test; then
			pass "anck_boot_test"
		else
			fail "anck_boot_test"
			return 1
		fi
	else
		fail "anck_rpm_build"
		skip "anck_boot_test"
		return 1
	fi
	return 0
}

parse()
{
	$TONE_BM_SUITE_DIR/parse.awk
}
