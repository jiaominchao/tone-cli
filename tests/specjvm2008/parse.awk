#!/usr/bin/awk -f
BEGIN {
    startup_helloworld = startup_compiler_compiler = startup_compress = startup_crypto_aes = startup_crypto_rsa = \
    startup_crypto_signverify = startup_mpegaudio = startup_scimark_fft = startup_scimark_lu = startup_scimark_monte_carlo = \
    startup_scimark_sor = startup_scimark_sparse = startup_serial = startup_sunflow = startup_xml_transform = \
    startup_xml_validation = compiler_compiler  = compress = crypto_aes = crypto_rsa = crypto_signverify = derby = \
    mpegaudio = scimark_fft_large = scimark_lu_large = scimark_sor_large = scimark_sparse_large = scimark_fft_small = \
    scimark_lu_small = scimark_sor_small = scimark_sparse_small = scimark_monte_carlo = serial = xml_transform = xml_validation = 0
}
/^Score on startup.helloworld:/ {
    startup_helloworld = $4
    startup_helloworld_unit = $5
}
/^Score on startup.compiler.compiler:/ {
    startup_compiler_compiler = $4
    startup_compiler_compiler_unit = $5
}
/^Score on startup.compress:/ {
    startup_compress = $4
    startup_compress_unit = $5
}
/^Score on startup.crypto.aes:/ {
    startup_crypto_aes = $4
    startup_crypto_aes_unit = $5
}
/^Score on startup.crypto.rsa:/ {
    startup_crypto_rsa = $4
    startup_crypto_rsa_unit = $5
}
/^Score on startup.crypto.signverify:/ {
    startup_crypto_signverify = $4
    startup_crypto_signverify_unit = $5
}
/^Score on startup.mpegaudio:/ {
    startup_mpegaudio = $4
    startup_mpegaudio_unit = $5
}
/^Score on startup.scimark.fft:/ {
    startup_scimark_fft = $4
    startup_scimark_fft_unit = $5
}
/^Score on startup.scimark.lu:/ {
    startup_scimark_lu = $4
    startup_scimark_lu_unit = $5
}
/^Score on startup.scimark.monte_carlo:/ {
    startup_scimark_monte_carlo = $4
    startup_scimark_monte_carlo_unit = $5
}
/^Score on startup.scimark.sor:/ {
    startup_scimark_sor = $4
    startup_scimark_sor_unit = $5
}
/^Score on startup.scimark.sparse:/ {
    startup_scimark_sparse = $4
    startup_scimark_sparse_unit = $5
}
/^Score on startup.serial:/ {
    startup_serial = $4
    startup_serial_unit = $5
}
/^Score on startup.sunflow:/ {
    startup_sunflow = $4
    startup_sunflow_unit = $5
}
/^Score on startup.xml.transform:/ {
    startup_xml_transform = $4
    startup_xml_transform_unit = $5
}
/^Score on startup.xml.validation:/ {
    startup_xml_validation = $4
    startup_xml_validation_unit = $5
}
/^Score on compiler.compiler:/ {
    compiler_compiler = $4
    compiler_compiler_unit = $5
}
/^Score on compress:/ {
    compress = $4
    compress_unit = $5
}
/^Score on crypto.aes:/ {
    crypto_aes = $4
    crypto_aes_unit = $5
}
/^Score on crypto.rsa:/ {
    crypto_rsa = $4
    crypto_rsa_unit = $5
}
/^Score on crypto.signverify:/ {
    crypto_signverify = $4
    crypto_signverify_unit = $5
}
/^Score on derby:/ {
    derby = $4
    derby_unit = $5
}
/^Score on mpegaudio:/ {
    mpegaudio = $4
    mpegaudio_unit = $5
}
/^Score on scimark.fft.large:/ {
    scimark_fft_large = $4
    scimark_fft_large_unit = $5
}
/^Score on scimark.lu.large:/ {
    scimark_lu_large = $4
    scimark_lu_large_unit = $5
}
/^Score on scimark.sor.large:/ {
    scimark_sor_large = $4
    scimark_sor_large_unit = $5
}
/^Score on scimark.sparse.large:/ {
    scimark_sparse_large = $4
    scimark_sparse_large_unit = $5
}
/^Score on scimark.fft.small:/ {
    scimark_fft_small = $4
    scimark_fft_small_unit = $5
}
/^Score on scimark.lu.small:/ {
    scimark_lu_small = $4
    scimark_lu_small_unit = $5
}
/^Score on scimark.sor.small:/ {
    scimark_sor_small = $4
    scimark_sor_small_unit = $5
}
/^Score on scimark.sparse.small:/ {
    scimark_sparse_small = $4
    scimark_sparse_small_unit = $5
}
/^Score on scimark.monte_carlo:/ {
    scimark_monte_carlo = $4
    scimark_monte_carlo_unit = $5
}
/^Score on serial:/ {
    serial = $4
    serial_unit = $5
}
/^Score on xml.transform:/ {
    xml_transform = $4
    xml_transform_unit = $5
}
/^Score on xml.validation:/ {
    xml_validation = $4
    xml_validation_unit = $5
}
END {
    printf("startup.helloworld: %f %s\n",startup_helloworld,startup_helloworld_unit)
    printf("startup.compiler.compiler: %f %s\n",startup_compiler_compiler,startup_compiler_compiler_unit)
    printf("startup.compress: %f %s\n",startup_compress,startup_compress_unit)
    printf("startup.crypto.aes: %f %s\n",startup_crypto_aes,startup_crypto_aes_unit)
    printf("startup.crypto.rsa: %f %s\n",startup_crypto_rsa,startup_crypto_rsa_unit)
    printf("startup.crypto.signverify: %f %s\n",startup_crypto_signverify,startup_crypto_signverify_unit)
    printf("startup.mpegaudio: %f %s\n",startup_mpegaudio,startup_mpegaudio_unit)
    printf("startup.scimark.fft: %f %s\n",startup_scimark_fft,startup_scimark_fft_unit)
    printf("startup.scimark.lu: %f %s\n",startup_scimark_lu,startup_scimark_lu_unit)
    printf("startup.scimark.monte_carlo: %f %s\n",startup_scimark_monte_carlo,startup_scimark_monte_carlo_unit)
    printf("startup.scimark.sor: %f %s\n",startup_scimark_sor,startup_scimark_sor_unit)
    printf("startup.scimark.sparse: %f %s\n",startup_scimark_sparse,startup_scimark_sparse_unit)
    printf("startup.serial: %f %s\n",startup_serial,startup_serial_unit)
    printf("startup.sunflow: %f %s\n",startup_sunflow,startup_sunflow_unit)
    printf("startup.xml.transform: %f %s\n",startup_xml_transform,startup_xml_transform_unit)
    printf("startup.xml.validation: %f %s\n",startup_xml_validation,startup_xml_validation_unit)
    printf("compiler.compiler: %f %s\n",compiler_compiler,compiler_compiler_unit)
    printf("compress: %f %s\n",compress,compress_unit)
    printf("crypto.aes: %f %s\n",crypto_aes,crypto_aes_unit)
    printf("crypto.rsa: %f %s\n",crypto_rsa,crypto_rsa_unit)
    printf("crypto.signverify: %f %s\n",crypto_signverify,crypto_signverify_unit)
    printf("derby: %f %s\n",derby,derby_unit)
    printf("mpegaudio: %f %s\n",mpegaudio,mpegaudio_unit)
    printf("scimark.fft.large: %f %s\n",scimark_fft_large,scimark_fft_large_unit)
    printf("scimark.lu.large: %f %s\n",scimark_lu_large,scimark_lu_large_unit)
    printf("scimark.sor.large: %f %s\n",scimark_sor_large,scimark_sor_large_unit)
    printf("scimark.sparse.large: %f %s\n",scimark_sparse_large,scimark_sparse_large_unit)
    printf("scimark.fft.small: %f %s\n",scimark_fft_small,scimark_fft_small_unit)
    printf("scimark.lu.small: %f %s\n",scimark_lu_small,scimark_lu_small_unit)
    printf("scimark.sor.small: %f %s\n",scimark_sor_small,scimark_sor_small_unit)
    printf("scimark.sparse.small: %f %s\n",scimark_sparse_small,scimark_sparse_small_unit)
    printf("scimark.monte_carlo: %f %s\n",scimark_monte_carlo,scimark_monte_carlo_unit)
    printf("serial: %f %s\n",serial,serial_unit)
    printf("xml.transform: %f %s\n",xml_transform,xml_transform_unit)
    printf("xml.validation: %f %s\n",xml_validation,xml_validation_unit)
}


