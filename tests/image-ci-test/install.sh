#!/bin/bash
# Avaliable environment:
#
# Download variable:
# WEB_URL=
GIT_URL="https://gitee.com/anolis/anolis-ci-test.git"
DEP_PKG_LIST="python3"


fetch()
{
    git_clone $GIT_URL $TONE_BM_CACHE_DIR
}



build()
{
    :
}

install()
{
    python3 -m pip install -r $TONE_BM_CACHE_DIR/image_ci/requirements.txt
    cp -Trf $TONE_BM_CACHE_DIR $TONE_BM_RUN_DIR
}

