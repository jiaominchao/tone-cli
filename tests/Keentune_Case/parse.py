#!/bin/env python3
import sys
import re

with sys.stdin as stdin:
    for line in stdin:
        match = re.search(r'tests/(.+);run-(.+)-.+:  (PASS|FAIL|SKIP|ERROR|WARN|INTERRUPT|CANCEL)', line)
        if match:
            group = match.groups()
            name, host, result = (group[0], group[1], group[2])
            if result in ['ERROR', 'INTERRUPT', 'CANCEL']:
                result = 'FAIL'
            if result is 'WARN':
                result = 'Warning'
            if 'py' in name:
                name = name.split(':')[1]
            print('localhost-{}: {}'.format(name, result.title()))

