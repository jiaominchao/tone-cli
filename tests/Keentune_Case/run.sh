#!/bin/bash
# Avaliable environment variables:
# $C_VERSION: image version seperated by '#'
# $CONTAINER_ENGINE: container engine for func test seperated by '/'


run()
{
    export PYTHONPATH=$TONE_BM_RUN_DIR/image_ci/tests
    registry_type=(`echo $REGISTRY_TYPE | tr '/' ' '`)

    if [ "$registry_type" == "app" ]; then
        export CONTAINER_ENGINE=docker
    fi
    generate_yaml
    #avocado -V run --nrunner-max-parallel-tasks 1 $TONE_BM_RUN_DIR/image_ci/tests/keentune/tc_container_${CONTAINER_SERVICE}_001.py --mux-yaml $TONE_BM_RUN_DIR/hosts.yaml -t container
    avocado -V run --nrunner-max-parallel-tasks 1 $TONE_BM_RUN_DIR/image_ci/tests/keentune --mux-yaml $TONE_BM_RUN_DIR/hosts.yaml -t container
    cp -rH $HOME/avocado/job-results/latest $TONE_CURRENT_RESULT_DIR
}

parse()
{
    $TONE_BM_SUITE_DIR/parse.py
}

generate_yaml()
{
    registry_addr=(`echo $REGISTRY_ADDR | tr '#' ' '`)
    c_engine=(`echo $CONTAINER_ENGINE | tr '/' ' '`)
cat > hosts.yaml << EOF
hosts: !mux
    localhost:
        host: localhost
registry: !mux
EOF
    for ((i = 0; i < ${#registry_addr[@]}; i++)); do
cat >> hosts.yaml << EOF
    registry$i:
        version: ${registry_addr[$i]}
EOF
    done

cat >> hosts.yaml << EOF
engines: !mux
EOF
    for ((i = 0; i < ${#c_engine[@]}; i++)); do
cat >> hosts.yaml << EOF
    engine$i:
        engine: ${c_engine[$i]}
EOF
    done
}
