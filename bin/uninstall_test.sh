#!/bin/bash

if [ -z "$1" ]; then
    cat <<EOF
Usage:
    $0 testsuite
EOF
    exit 1
fi

testsuite=$1
[ -n "$TONE_ROOT" ] || TONE_ROOT=$(dirname $(dirname `readlink -f $0`))

. $TONE_ROOT/lib/common.sh

if [ -e ./var.sh ]; then
    . ./var.sh
elif [ -e $TONE_CURRENT_RESULT_DIR/var.sh ]; then
    . $TONE_CURRENT_RESULT_DIR/var.sh
else
    echo "Error: no test suit setting (./var.sh)"
    exit 1
fi

. $TONE_BM_SUITE_DIR/install.sh

cd $TONE_CURRENT_RESULT_DIR
uninstall
